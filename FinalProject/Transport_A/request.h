#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "stop.h"
#include "transport_manager.h"

struct Request;
using RequestHolder = std::unique_ptr<Request>;

struct Request {
    enum class Type {
	ADD_STOP,
	ADD_ROUTE,
	READ_ROUTE,
	WRONG_COMMAND
    };
  
    Request(Type type) : type(type) {}
    static RequestHolder Create(Type type);
    virtual void ParseFrom(std::string_view input) = 0;
    virtual ~Request() = default;

    const Type type;
};

template <typename ResultType>
struct ReadRequest : Request {
    using Request::Request;
    virtual ResultType Process(const TransportManager& manager) const = 0;
};

struct ModifyRequest : Request {
    using Request::Request;
    virtual void Process(TransportManager& manager) const = 0;
};

struct ReadRouteRequest : ReadRequest<std::string> {
    ReadRouteRequest() : ReadRequest(Type::READ_ROUTE) {}
    void ParseFrom(std::string_view input) override; 
    std::string Process(const TransportManager& manager) const;
    
    std::string name;
};

struct AddStopRequest : ModifyRequest {
    AddStopRequest() : ModifyRequest(Type::ADD_STOP) {}
    void ParseFrom(std::string_view input) override;
    void Process(TransportManager& manager) const override;
        
    std::string name;
    double lat;
    double lon;
};

struct AddRouteRequest : ModifyRequest {
    AddRouteRequest() : ModifyRequest(Type::ADD_ROUTE) {}
    void ParseFrom(std::string_view input) override;
    void Process(TransportManager& manager) const override;
    void ParseStops(std::string_view input, char delimiter);
    
    std::string name;
    std::vector<std::string> stops;
    bool isCircular;
};

std::vector<RequestHolder> ReadRequests(size_t request_count,
					std::istream& in_stream = std::cin);

std::vector<std::string> ProcessRequests(TransportManager& manager,
					 const std::vector<RequestHolder>& requests);

void PrintResponses(const std::vector<std::string>& responses,
		    std::ostream& stream = std::cout);
