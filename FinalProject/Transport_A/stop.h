#pragma once

#include <cmath>
#include <string>

class Stop {
public:    
    explicit Stop(const std::string& name,
		  double latitude,
		  double longitude) :
        areCoordinatesSet_(true),
	name_(name),
	latitude_(latitude * 3.1415926535/180.),
	    longitude_(longitude * 3.1415926535/180.) {};
	
    explicit Stop(const std::string& name) :
        areCoordinatesSet_(false),
	name_(name),
	latitude_(0.),
	    longitude_(0.) {};
	
    void SetLatitudeAndLongitude(double lat, double lon);
    bool isSet() const;
    double CalculateDistance(const Stop& other) const;
    void Print() const; 
    
private:
    bool areCoordinatesSet_;
    std::string name_;
    double latitude_;
    double longitude_;
};
