#include <iomanip>
#include <sstream>

#include "json.h"
#include "request.h"

using namespace std;
using namespace Json;

void ReadRouteRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node ReadRouteRequest::ProcessJSON(const TransportManager& manager) const {
    return manager.DescribeRouteJSON(name, id);
}

void ReadStopRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node ReadStopRequest::ProcessJSON(const TransportManager& manager) const{
    return manager.DescribeStopJSON(name, id);
}

void CalcPathRequest::ParseFromJSON(const Node& node) {
    from = node.AsMap().at("from").AsString();
    to = node.AsMap().at("to").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node CalcPathRequest::ProcessJSON(const TransportManager& manager) const{
    return manager.DescribePathJSON(from, to, id);
}

void AddStopRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    for (const auto& n: node.AsMap().at("road_distances").AsMap()) {
	distances[n.first] = n.second.AsInt();
    }
    lat = node.AsMap().at("latitude").AsDouble();
    lon = node.AsMap().at("longitude").AsDouble();
}

void AddStopRequest::ProcessJSON(TransportManager& manager) const {
    manager.AddStop(name, lat, lon, distances);
}

void AddRouteRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    isCircular = node.AsMap().at("is_roundtrip").AsBool();
    for (const auto& n: node.AsMap().at("stops").AsArray()) {
	stops.push_back(n.AsString());
    }
}

void AddRouteRequest::ProcessJSON(TransportManager& manager) const {
    manager.AddRoute(name, stops, isCircular);
}

void SetParametersRequest::ParseFromJSON(const Node& node) {
    wait_time = node.AsMap().at("bus_wait_time").AsInt();
    velocity = node.AsMap().at("bus_velocity").AsDouble();
//    cout << wait_time << " " << velocity << endl; 
}

void SetParametersRequest::ProcessJSON(TransportManager& manager) const {
    manager.SetWaitTime(wait_time);
    manager.SetVelocity(velocity);
//    cout << "Process " << manager.GetWaitTime() << " "
//	 << manager.GetVelocity() << endl;
}

vector<RequestHolder> ReadRequestsJSON(istream& in_stream) {
    vector<RequestHolder> requests;
    
    auto doc = Load(in_stream);
    auto& req = doc.GetRoot().AsMap();
    
    const string settings_type = "routing_settings";
    const auto& settings = req.at(settings_type);
    
    RequestHolder request = Request::Create(Request::Type::SET_PARAM);
    if (request) {
	request->ParseFromJSON(settings);
    };
    requests.push_back(move(request));
	
    const string modify_type = "base_requests";
    const auto& base_req = req.at(modify_type).AsArray();

    for (const auto& node : base_req) {
	string type = node.AsMap().at("type").AsString();
	Request::Type rtype;
	if (type == "Stop") {
	    rtype = Request::Type::ADD_STOP;
	}
	else if (type == "Bus") {
	    rtype = Request::Type::ADD_ROUTE;
	}
	else {
	    continue;
	}
	RequestHolder request = Request::Create(rtype);
	
	if (request) {
	    request->ParseFromJSON(node);
	};
	requests.push_back(move(request));
    }

    const string read_type = "stat_requests";
    const auto& stat_req = req.at(read_type).AsArray();

    for (const auto& node : stat_req) {
	string type = node.AsMap().at("type").AsString();
	Request::Type rtype;
	if (type == "Stop") {
	    rtype = Request::Type::READ_STOP;
	}
	else if (type == "Bus") {
	    rtype = Request::Type::READ_ROUTE;
	}
	else if (type == "Route") {
	    rtype = Request::Type::CALC_PATH;
	}
	else {
	    continue;
	}
	RequestHolder request = Request::Create(rtype);
	
	if (request) {
	    request->ParseFromJSON(node);
	};
	requests.push_back(move(request));
    }
        
    return requests;
}

vector<Node> ProcessRequestsJSON(TransportManager& manager,
			       const vector<RequestHolder>& requests) {
    vector<Node> responses;
    
    for (const auto& request_holder : requests) {
	if (request_holder->type == Request::Type::READ_ROUTE) {
	    const auto& request = static_cast<const ReadRouteRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else if (request_holder->type == Request::Type::READ_STOP) {
	    const auto& request = static_cast<const ReadStopRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else if (request_holder->type == Request::Type::CALC_PATH) {
	    const auto& request = static_cast<const CalcPathRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else {
	    const auto& request = static_cast<const ModifyRequest&>(*request_holder);
	    request.ProcessJSON(manager);
	}
    }
    return responses;
}

void PrintResponsesJSON(const vector<Node>& responses,
			ostream& stream) {
    stream << fixed << setprecision(16);
    PrintNode(responses, stream);
}
