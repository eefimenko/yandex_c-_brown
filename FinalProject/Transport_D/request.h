#pragma once

#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "json.h"
#include "stop.h"
#include "transport_manager.h"

struct Request;
using RequestHolder = std::unique_ptr<Request>;

struct Request {
    enum class Type {
	ADD_ROUTE,
	ADD_STOP,
	READ_ROUTE,
	READ_STOP
    };
  
    Request(Type type) : type(type) {}
    static RequestHolder Create(Type type);
    virtual void ParseFrom(std::string_view input) = 0;
    virtual void ParseFromJSON(const Json::Node& node) = 0;
    virtual ~Request() = default;

    const Type type;
    std::string name;
};

template <typename ResultType>
struct ReadRequest : Request {
    using Request::Request;
    virtual ResultType Process(const TransportManager& manager) const = 0;
};

struct ModifyRequest : Request {
    using Request::Request;
    virtual void Process(TransportManager& manager) const = 0;
    virtual void ProcessJSON(TransportManager& manager) const = 0;
};

struct ReadRouteRequest : ReadRequest<std::string> {
    ReadRouteRequest() : ReadRequest(Type::READ_ROUTE) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;  
    std::string Process(const TransportManager& manager) const;
    Json::Node ProcessJSON(const TransportManager& manager) const;
    int id;
};

struct ReadStopRequest : ReadRequest<std::string> {
    ReadStopRequest() : ReadRequest(Type::READ_STOP) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override; 
    std::string Process(const TransportManager& manager) const;
    Json::Node ProcessJSON(const TransportManager& manager) const;
    
    int id;
};

struct AddStopRequest : ModifyRequest {
    AddStopRequest() : ModifyRequest(Type::ADD_STOP) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;
    void Process(TransportManager& manager) const override;
    void ProcessJSON(TransportManager& manager) const override;

    double lat;
    double lon;
    std::map<std::string, int> distances;
};

struct AddRouteRequest : ModifyRequest {
    AddRouteRequest() : ModifyRequest(Type::ADD_ROUTE) {}
    void ParseFrom(std::string_view input) override;
    void ParseFromJSON(const Json::Node& node) override;
    void Process(TransportManager& manager) const override;
    void ProcessJSON(TransportManager& manager) const override;
    void ParseStops(std::string_view input, char delimiter);

    std::vector<std::string> stops;
    bool isCircular;
};

std::vector<RequestHolder> ReadRequests(
    std::istream& in_stream = std::cin);

std::vector<RequestHolder> ReadRequestsJSON(
    std::istream& in_stream = std::cin);

std::vector<std::string> ProcessRequests(
    TransportManager& manager,
    const std::vector<RequestHolder>& requests);

std::vector<Json::Node> ProcessRequestsJSON(
    TransportManager& manager,
    const std::vector<RequestHolder>& requests);

void PrintResponses(const std::vector<std::string>& responses,
		    std::ostream& stream = std::cout);

void PrintResponsesJSON(const std::vector<Json::Node>& responses,
		    std::ostream& stream = std::cout);
