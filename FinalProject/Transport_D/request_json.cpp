#include <iomanip>
#include <sstream>

#include "json.h"
#include "request.h"

using namespace std;
using namespace Json;

void ReadRouteRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node ReadRouteRequest::ProcessJSON(const TransportManager& manager) const {
    return manager.DescribeRouteJSON(name, id);
}

void ReadStopRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    id = node.AsMap().at("id").AsInt();
}

Node ReadStopRequest::ProcessJSON(const TransportManager& manager) const{
    return manager.DescribeStopJSON(name, id);
}

void AddStopRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    for (const auto& n: node.AsMap().at("road_distances").AsMap()) {
	//cout << n.first << " " << n.second.AsInt() << endl;
	distances[n.first] = n.second.AsInt();
    }
    lat = node.AsMap().at("latitude").AsDouble();
    //cout << fixed << setprecision(6) << lat << endl;
    lon = node.AsMap().at("longitude").AsDouble();
    //cout << fixed << setprecision(6) << lon << endl;
}

void AddStopRequest::ProcessJSON(TransportManager& manager) const {
    manager.AddStop(name, lat, lon, distances);
}

void AddRouteRequest::ParseFromJSON(const Node& node) {
    name = node.AsMap().at("name").AsString();
    isCircular = node.AsMap().at("is_roundtrip").AsBool();
    for (const auto& n: node.AsMap().at("stops").AsArray()) {
	stops.push_back(n.AsString());
    }
}

void AddRouteRequest::ProcessJSON(TransportManager& manager) const {
    manager.AddRoute(name, stops, isCircular);
}

vector<RequestHolder> ReadRequestsJSON(istream& in_stream) {
    vector<RequestHolder> requests;
    
    auto doc = Load(in_stream);
    auto& req = doc.GetRoot().AsMap();

    const string modify_type = "base_requests";
    const auto& base_req = req.at(modify_type).AsArray();

    for (const auto& node : base_req) {
	string type = node.AsMap().at("type").AsString();
	Request::Type rtype;
	if (type == "Stop") {
	    rtype = Request::Type::ADD_STOP;
	}
	else if (type == "Bus") {
	    rtype = Request::Type::ADD_ROUTE;
	}
	else {
	    continue;
	}
	RequestHolder request = Request::Create(rtype);
	
	if (request) {
	    request->ParseFromJSON(node);
	};
	requests.push_back(move(request));
    }

    const string read_type = "stat_requests";
    const auto& stat_req = req.at(read_type).AsArray();

    for (const auto& node : stat_req) {
	string type = node.AsMap().at("type").AsString();
	Request::Type rtype;
	if (type == "Stop") {
	    rtype = Request::Type::READ_STOP;
	}
	else if (type == "Bus") {
	    rtype = Request::Type::READ_ROUTE;
	}
	else {
	    continue;
	}
	RequestHolder request = Request::Create(rtype);
	
	if (request) {
	    request->ParseFromJSON(node);
	};
	requests.push_back(move(request));
    }
        
    return requests;
}

vector<Node> ProcessRequestsJSON(TransportManager& manager,
			       const vector<RequestHolder>& requests) {
    vector<Node> responses;
    
    for (const auto& request_holder : requests) {
	if (request_holder->type == Request::Type::READ_ROUTE) {
	    const auto& request = static_cast<const ReadRouteRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	}
	else if (request_holder->type == Request::Type::READ_STOP) {
	    const auto& request = static_cast<const ReadStopRequest&>(*request_holder);
	    responses.push_back(request.ProcessJSON(manager));
	} else {
	    const auto& request = static_cast<const ModifyRequest&>(*request_holder);
	    request.ProcessJSON(manager);
	}
    }
    return responses;
}

void PrintResponsesJSON(const vector<Node>& responses,
			ostream& stream) {
    stream << fixed << setprecision(16);
    PrintNode(responses, stream);
}
