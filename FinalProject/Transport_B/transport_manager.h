#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

#include "stop.h"
#include "route.h"

class TransportManager {
public:
    TransportManager() = default;
    
    std::shared_ptr<Stop> AddStop(const std::string& name,
				  double lat,
				  double lon);
    std::shared_ptr<Stop> AddStop(const std::string& name,
				  const std::string& route_name);
    
    void AddRoute(const std::string& name,
		  const std::vector<std::string>& stops,
		  bool isCircular);

    std::ostream& DescribeRoute(const std::string& name,
				std::ostream& out) const;
    std::ostream& DescribeStop(const std::string& name,
			       std::ostream& out) const;
    
    void Print() const;
	
private:
    std::unordered_map<std::string, std::shared_ptr<Stop>> stops;
    std::unordered_map<std::string, std::shared_ptr<Route>> routes;
};
