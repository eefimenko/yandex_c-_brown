#include <iostream>
#include <string>

#include "stop.h"

using namespace std;

void Stop::SetLatitudeAndLongitude(double latitude, double longitude) {
    latitude_ = latitude * 3.1415926535/180.;
    longitude_ = longitude * 3.1415926535/180.;
    areCoordinatesSet_ = true;
};
    
bool Stop::isSet() const {
    return areCoordinatesSet_;
}
    
double Stop::CalculateDistance(const Stop& other) const {
    return acos(sin(latitude_) * sin(other.latitude_) +
		cos(latitude_) * cos(other.latitude_) *
		cos(fabs(longitude_ - other.longitude_))) * 6371000;
}

const set<string>& Stop::GetRoutes() const {
    return routes_;
};

void Stop::AddRoute(const std::string& route_name) {
    routes_.insert(route_name);
};

void Stop::Print() const {
    cout << "Name: " << name_
	 << " set: " << areCoordinatesSet_;
    if (areCoordinatesSet_) {
	cout << " lat: " << latitude_
	     << " lon: " << longitude_;
    }
    cout << endl;
}
