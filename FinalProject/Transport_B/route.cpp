#include <iostream>
#include <memory>
#include <unordered_set>

#include "route.h"

using namespace std;

void Route::AddStop(std::shared_ptr<Stop> stop) {
    stops_.push_back(stop);
}
    
double Route::GetLength() const {
    return length_;
}

int Route::GetNumberOfStops() const {
    return n_stops_;
}

int Route::GetNumberOfUniqueStops() const {
    return n_unique_;
}

void Route::Print() const {
    for (const auto& p : stops_) {
	p->Print();
    }
    cout << endl;
}

double Route::CalculateLength() {
    if (length_ == -1 && !stops_.empty()) {
	length_ = 0.;
	auto p1 = stops_.begin();
	auto p2 = next(p1);
	while (p2 != stops_.end()) {
	    length_ += p1->get()->CalculateDistance(*p2->get());
	    p1 = p2;
	    p2 = next(p2);
	}
	
	n_stops_ = stops_.size();
	if (!isCircular_) {
	    length_ *= 2.;
	    n_stops_ = 2 * n_stops_ - 1;
	}
	
	unordered_set<shared_ptr<Stop>> unique(stops_.begin(), stops_.end());
	n_unique_ = unique.size();
    }
    return length_;
}

