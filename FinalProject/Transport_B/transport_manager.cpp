#include <iostream>
#include <memory>
#include <string>

#include "stop.h"
#include "transport_manager.h"

using namespace std;

shared_ptr<Stop> TransportManager::AddStop(const string& name,
					   double lat,
					   double lon) {
    shared_ptr<Stop> result = nullptr;
    
    auto it = stops.find(name);
    if (it != stops.end()) {
	if (!it->second->isSet()) {
	    it->second->SetLatitudeAndLongitude(lat, lon);
	}
	result = it->second;
    }
    else {
	result = make_shared<Stop>(name, lat, lon);
	stops[name] = result;
    }
    return result;
}

shared_ptr<Stop> TransportManager::AddStop(const string& name,
					   const string& route_name) {
    shared_ptr<Stop> result = nullptr;
    
    auto it = stops.find(name);
    if (it != stops.end()) {
	result = it->second;
    }
    else {
	result = make_shared<Stop>(name);
	stops[name] = result;
    }

    result->AddRoute(route_name);

    return result;
}

void TransportManager::AddRoute(const string& name,
				const vector<string>& stops,
				bool isCircular) {
    auto it = routes.find(name);
    
    if (it == routes.end()) {
	routes[name] = make_shared<Route>(name, isCircular);
	for (const auto& stop_name : stops) {
	    routes[name]->AddStop(AddStop(stop_name, name));
	}
    }
}

ostream& TransportManager::DescribeRoute(const string& name,
					 ostream& out) const {
    auto it = routes.find(name);
    if (it == routes.end()) {
	out << "Bus " << name << ": not found";
    }
    else {
	double length = it->second->CalculateLength();
	out << "Bus " << name << ": " << it->second->GetNumberOfStops()
	    << " stops on route, " << it->second->GetNumberOfUniqueStops()
	    << " unique stops, " << length << " route length";
    }
    return out;
}

ostream& TransportManager::DescribeStop(const string& name,
					ostream& out) const {
    auto it = stops.find(name);

    if (it == stops.end()) {
	out << "Stop " << name << ": not found";
    }
    else {
	out << "Stop " << name << ": ";
	auto rs = it->second->GetRoutes();

	if (rs.empty()) {
	    out << "no buses";
	}
	else {
	    out << "buses ";
	    auto it_ = rs.begin();
	    for (; it_ != prev(rs.end()); ++it_) {
		out << *it_ << " ";
	    }
	    out << *it_;
	}
    }
    return out;
}

void TransportManager::Print() const {
    cout << "Stops" << endl;
    for (const auto& p : stops) {
	p.second->Print();
    }

    cout << "Routes" << endl;
    for (const auto& p : routes) {
	p.second->Print();
    }
}
