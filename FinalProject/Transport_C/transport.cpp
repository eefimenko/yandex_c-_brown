#include <iostream>
#include <vector>

#include "request.h"

using namespace std;

int main() {
    cout.precision(6);
    size_t number;
    TransportManager manager;
    
    while (cin >> number) {
	const auto requests = ReadRequests(number);
	const auto responses = ProcessRequests(manager, requests);
	PrintResponses(responses);
    }
    return 0;
}
