#include "test_runner.h"

#include <iostream>
#include <map>
#include <string>
#include <unordered_map>

using namespace std;

struct Record {
    string id;
    string title;
    string user;
    int timestamp;
    int karma;
};

struct DBRecord {
    DBRecord(const Record& record) : rec(record) {};
    Record rec;
    multimap<int, Record*>::iterator tIt;
    multimap<int, Record*>::iterator kIt;
    multimap<string, Record*>::iterator uIt;
};

// Реализуйте этот класс
class Database {
public:
    bool Put(const Record& record)
    {
	auto [it, status] = data.emplace(pair<string, DBRecord>(record.id, {record}));

	if (!status)
	{
	    return false;
	}

	Record* pointer = &(it->second.rec);
	
	it->second.tIt = tIndex.emplace(pair<int, Record*>(record.timestamp, pointer));
	it->second.kIt = kIndex.emplace(pair<int, Record*>(record.karma, pointer));
	it->second.uIt = uIndex.emplace(pair<string, Record*>(record.user, pointer));
	
	return true;
    };
    
    const Record* GetById(const string& id) const
    {
	auto it = data.find(id);
	if (it == data.end())
	{
	    return nullptr;
	}
	return &(it->second.rec);
    };
    
    bool Erase(const string& id)
    {
	auto it = data.find(id);
	if (it == data.end())
	{
	    return false;
	}

	tIndex.erase(it->second.tIt);
	kIndex.erase(it->second.kIt);
	uIndex.erase(it->second.uIt);
	
	data.erase(it);
	return true;
    };

    template <typename Callback>
    void RangeByTimestamp(int low, int high, Callback callback) const
    {
	auto tbegin = tIndex.lower_bound(low);
	auto tend = tIndex.upper_bound(high);
	for (auto it_ = tbegin; it_ != tend; ++it_)
	{
	    if (!callback(*(it_->second)))
	    {
		break;
	    }
	}
    };

    template <typename Callback>
    void RangeByKarma(int low, int high, Callback callback) const {
	auto kbegin = kIndex.lower_bound(low);
	auto kend = kIndex.upper_bound(high);
	for (auto it_ = kbegin; it_ != kend; ++it_)
	{
	    if (!callback(*(it_->second)))
	    {
		break;
	    }
	}
    };

    template <typename Callback>
    void AllByUser(const string& user, Callback callback) const
    {
	auto [ubegin, uend] = uIndex.equal_range(user);
	
	for (auto it_ = ubegin; it_ != uend; ++it_)
	{
	    if (!callback(*(it_->second)))
	    {
		break;
	    }
	}
    };
    
private:
    unordered_map<string, DBRecord> data;
    
    multimap<int, Record*> tIndex;
    multimap<int, Record*> kIndex;
    multimap<string, Record*> uIndex;
};

void TestRangeBoundaries() {
    const int good_karma = 1000;
    const int bad_karma = -10;

    Database db;
    db.Put({"id1", "Hello there", "master", 1536107260, good_karma});
    db.Put({"id2", "O>>-<", "general2", 1536107260, bad_karma});

    int count = 0;
    db.RangeByKarma(bad_karma, good_karma, [&count](const Record&) {
	    ++count;
	    return true;
	});

    ASSERT_EQUAL(2, count);
}

void TestSameUser() {
    Database db;
    db.Put({"id1", "Don't sell", "master", 1536107260, 1000});
    db.Put({"id2", "Rethink life", "master", 1536107260, 2000});

    int count = 0;
    db.AllByUser("master", [&count](const Record&) {
	    ++count;
	    return true;
	});

    ASSERT_EQUAL(2, count);
}

void TestReplacement() {
    const string final_body = "Feeling sad";

    Database db;
    db.Put({"id", "Have a hand", "not-master", 1536107260, 10});
    db.Erase("id");
    db.Put({"id", final_body, "not-master", 1536107260, -10});

    auto record = db.GetById("id");
    ASSERT(record != nullptr);
    ASSERT_EQUAL(final_body, record->title);
}

int main() {
    TestRunner tr;
    RUN_TEST(tr, TestRangeBoundaries);
    RUN_TEST(tr, TestSameUser);
    RUN_TEST(tr, TestReplacement);
    return 0;
}
