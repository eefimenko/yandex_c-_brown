#include <iostream>
#include <map>
#include <string>
#include <unordered_map>

#include "tasks.h"

using namespace std;

class TeamTasks {
public:
    // Получить статистику по статусам задач конкретного разработчика
    const TasksInfo& GetPersonTasksInfo(const string& person) const {
	return data_.at(person);
    };
  
    // Добавить новую задачу (в статусе NEW) для конкретного разработчитка
    void AddNewTask(const string& person) {
	data_[person][TaskStatus::NEW]++;
    };
  
    // Обновить статусы по данному количеству задач конкретного разработчика,
    // подробности см. ниже
    tuple<TasksInfo, TasksInfo> PerformPersonTasks(
	const string& person, int task_count) {
	TasksInfo updated;
	TasksInfo not_updated;

	int in_new = data_[person][TaskStatus::NEW];
	int in_progress = data_[person][TaskStatus::IN_PROGRESS];
	int in_testing = data_[person][TaskStatus::TESTING];
	int in_done = data_[person][TaskStatus::DONE];
	int unfinished_tasks = in_new + in_progress + in_testing;
	  	
	if (task_count > unfinished_tasks) {
	    task_count = unfinished_tasks;
	}
	
	int moved_from_new = task_count > in_new ? in_new : task_count;
	task_count -= moved_from_new;
	int moved_from_in_progress = task_count > in_progress ? in_progress : task_count;
	task_count -= moved_from_in_progress;
	int moved_from_testing = task_count > in_testing ? in_testing : task_count;
	
	if (moved_from_new > 0) {
	    if (in_new - moved_from_new > 0) {
		not_updated[TaskStatus::NEW] = in_new - moved_from_new;
	    }
	}
	else {
	    if (in_new > 0) {
		not_updated[TaskStatus::NEW] = in_new;
	    }
	}

	if (moved_from_new > 0 || moved_from_in_progress > 0) {
	    if (moved_from_new > 0) {
		updated[TaskStatus::IN_PROGRESS] = moved_from_new;
	    }
	    if (in_progress - moved_from_in_progress > 0) {
		not_updated[TaskStatus::IN_PROGRESS] = in_progress- moved_from_in_progress;
	    }
	}
	else {
	    if (in_progress > 0) {
		not_updated[TaskStatus::IN_PROGRESS] = in_progress;
	    }
	}
	
	if (moved_from_testing > 0 || moved_from_in_progress > 0) {
	    if (moved_from_in_progress > 0) {
		updated[TaskStatus::TESTING] = moved_from_in_progress;
	    }
	    if (in_testing - moved_from_testing > 0) {
		not_updated[TaskStatus::TESTING] = in_testing - moved_from_testing;
	    }
	}
	else {
	    if (in_testing > 0) {
		not_updated[TaskStatus::TESTING] = in_testing;
	    }
	}

	if (moved_from_testing > 0) {
	    updated[TaskStatus::DONE] = moved_from_testing;
	}
	/*else {
	    if (in_done > 0) {
		not_updated[TaskStatus::DONE] = in_done;
	    }
	    }*/
	data_[person][TaskStatus::NEW] = in_new - moved_from_new;
	data_[person][TaskStatus::IN_PROGRESS] = in_progress
	    + moved_from_new
	    - moved_from_in_progress;
	data_[person][TaskStatus::TESTING] = in_testing
	    + moved_from_in_progress
	    - moved_from_testing;
	data_[person][TaskStatus::DONE] = in_done
	    + moved_from_testing;
	return {updated, not_updated};
    };
private:
    unordered_map<string, TasksInfo> data_;
};

// Принимаем словарь по значению, чтобы иметь возможность
// обращаться к отсутствующим ключам с помощью [] и получать 0,
// не меняя при этом исходный словарь
void PrintTasksInfo(TasksInfo tasks_info) {
    cout << tasks_info[TaskStatus::NEW]
	 << " new tasks"  << ", "
	 << tasks_info[TaskStatus::IN_PROGRESS]
	 << " tasks in progress" << ", "
	 << tasks_info[TaskStatus::TESTING]
	 << " tasks are being tested" << ", "
	 << tasks_info[TaskStatus::DONE]
	 << " tasks are done" << endl;
}

int main() {
    TeamTasks tasks;
    tasks.AddNewTask("Ilia");
    for (int i = 0; i < 3; ++i) {
	tasks.AddNewTask("Ivan");
    }
    cout << "Ilia's tasks: ";
    PrintTasksInfo(tasks.GetPersonTasksInfo("Ilia"));
    cout << "Ivan's tasks: ";
    PrintTasksInfo(tasks.GetPersonTasksInfo("Ivan"));
  
    TasksInfo updated_tasks, untouched_tasks;
  
    tie(updated_tasks, untouched_tasks) =
	tasks.PerformPersonTasks("Ivan", 2);
    cout << "Updated Ivan's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Ivan's tasks: ";
    PrintTasksInfo(untouched_tasks);
  
    tie(updated_tasks, untouched_tasks) =
	tasks.PerformPersonTasks("Ivan", 2);
    cout << "Updated Ivan's tasks: ";
    PrintTasksInfo(updated_tasks);
    cout << "Untouched Ivan's tasks: ";
    PrintTasksInfo(untouched_tasks);

    return 0;
}
