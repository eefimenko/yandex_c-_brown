#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <string_view>
#include <vector>

#include "test_runner.h"

using namespace std;

bool IsSubdomain(string_view subdomain, string_view domain) {
    if (subdomain == domain) {
	return true;
    }
    int i = subdomain.size() - 1;
    int j = domain.size() - 1;
    while (i >= 0 && j >= 0) {
	if (subdomain[i--] != domain[j--]) {
	    return false;
	}
    }
    return (j < 0 && subdomain[i] == '.');
}

bool IsReverseSubdomain(string_view subdomain, string_view domain) {
    if (subdomain == domain) {
	return true;
    }
    
    size_t i = 0;
    size_t j = 0;
    while (i < subdomain.size() && j < domain.size()) {
	if (subdomain[i++] != domain[j++]) {
	    return false;
	}
    }
    return (j >= domain.size() && subdomain[i] == '.');
}

void ReverseAndSortDomains(vector<string>& domains) {
    for (string& domain : domains) {
	reverse(begin(domain), end(domain));
    }
    sort(begin(domains), end(domains));
}

void FilterOutSubdomains(vector<string>& domains) {
    size_t insert_pos = 0;
    for (string& domain : domains) {
	if (insert_pos == 0 || !IsReverseSubdomain(domain, domains[insert_pos - 1])) {
	    swap(domains[insert_pos++], domain);
	}
    }
    domains.resize(insert_pos);
}

vector<string> ReadDomains(istream& in) {
  size_t count;
  in >> count;

  vector<string> domains;
  for (size_t i = 0; i < count; ++i) {
    string domain;
    in >> domain;
    domains.push_back(domain);
  }
  return domains;
}

void PrintDomains(vector<string>& domains, const string& title) {
    cout << title << ": ";
    for (const auto& d : domains) {
	cout << d << " ";
    }
    cout << endl;
}

void TestReadDomains() {
    vector<string> output = {"one", "two", "three"};
    string input = "3\none\ntwo\nthree";
    stringstream in(input);
    ASSERT_EQUAL(ReadDomains(in), output);
}

void TestIsSubdomain() {
    {
	string domain = "ya.ru";
	string subdomain = "m.ya.ru";
	ASSERT_EQUAL(IsSubdomain(subdomain, domain), true);
	ASSERT_EQUAL(IsSubdomain(domain, subdomain), false);
    }
    {
	string domain = "ya.ru";
	string subdomain = "mya.ru";
	ASSERT_EQUAL(IsSubdomain(subdomain, domain), false);
    }
}

void TestIsReverseSubdomain() {
    {
	string domain = "ur.ay";
	string subdomain = "ur.ay.m";
	ASSERT_EQUAL(IsReverseSubdomain(subdomain, domain), true);
    }
    {
	string domain = "ur.ay";
	string subdomain = "ur.ay";
	ASSERT_EQUAL(IsReverseSubdomain(subdomain, domain), true);
    }
    {
	string domain = "ur.ay";
	string subdomain = "ur.aym";
	ASSERT_EQUAL(IsReverseSubdomain(subdomain, domain), false);
    }
    {
	string domain = "moc";
	string subdomain = "moc.ay";
	ASSERT_EQUAL(IsReverseSubdomain(subdomain, domain), true);
    }
}

void TestFilterOut() {
    vector<string> input = {"ur.ay", "ur.ay.m", "ur.aym", "moc", "moc.liam"};
    vector<string> expected = {"ur.ay", "ur.aym", "moc"};
    FilterOutSubdomains(input);
    ASSERT_EQUAL(input, expected);
}

int main() {
    TestRunner tr;
    RUN_TEST(tr, TestReadDomains);
    RUN_TEST(tr, TestIsSubdomain);
    RUN_TEST(tr, TestIsReverseSubdomain);
    RUN_TEST(tr, TestFilterOut);
       
    vector<string> banned_domains = ReadDomains(cin);
    vector<string> domains_to_check = ReadDomains(cin);

    PrintDomains(banned_domains, "Banned");
    PrintDomains(domains_to_check, "Check");
        
    ReverseAndSortDomains(banned_domains);
    FilterOutSubdomains(banned_domains);
    PrintDomains(banned_domains, "Reversed and filtered banned");
    
    for (string& domain : domains_to_check) {
	reverse(begin(domain), end(domain));
	const auto it = upper_bound(begin(banned_domains), end(banned_domains), domain);
	if (it == begin(banned_domains) || (it != begin(banned_domains) && !IsReverseSubdomain(domain, *prev(it)))) {
	    cout << "Good" << endl;
	} else {
	    cout << "Bad" << endl;
	}
    }
    return 0;
}
