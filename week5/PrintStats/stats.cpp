#include <algorithm>
#include <iostream>
#include <vector>

#include "stats.h"

using namespace std;

template <typename InputIt>
int ComputeMedianAge(InputIt range_begin, InputIt range_end) {
    if (range_begin == range_end) {
	return 0;
    }
  
    vector<typename InputIt::value_type> range_copy(range_begin, range_end);
    auto middle = begin(range_copy) + range_copy.size() / 2;
    nth_element(
	begin(range_copy), middle, end(range_copy),
	[](const Person& lhs, const Person& rhs) {
	    return lhs.age < rhs.age;
	}
	);
    return middle->age;
}

void PrintStats(vector<Person> persons) {
    auto females_end = partition(
	persons.begin(),
	persons.end(),
	[](const Person& p) { return p.gender == Gender::FEMALE;});
    auto empl_females_end = partition(
	persons.begin(),
	females_end,
	[](const Person& p) {return p.is_employed;});
    auto empl_males_end = partition(
	females_end,
	persons.end(),
	[](const Person& p) {return p.is_employed;});
    
    cout << "Median age = "
	 << ComputeMedianAge(persons.begin(), persons.end())
	 << endl;
    
    cout << "Median age for females = "
	 << ComputeMedianAge(persons.begin(), females_end)
	 << endl;
    
    cout << "Median age for males = "
	 << ComputeMedianAge(females_end, persons.end())
	 << endl;

    cout << "Median age for employed females = "
	 << ComputeMedianAge(persons.begin(), empl_females_end)
	 << endl;
    
    cout << "Median age for unemployed females = "
	 << ComputeMedianAge(empl_females_end, females_end)
	 << endl;

    cout << "Median age for employed males = "
	 << ComputeMedianAge(females_end, empl_males_end)
	 << endl;
    cout << "Median age for unemployed males = "
	 << ComputeMedianAge(empl_males_end, persons.end())
	 << endl;
}
