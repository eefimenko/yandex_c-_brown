#include <algorithm>
#include <charconv>
#include <ctime>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

using namespace std;

string_view extractInt(string_view s, int& value, char delimiter)
{
    size_t pos = s.find(delimiter);
    string_view svalue = s.substr(0, pos);
    auto result = from_chars(svalue.data(), svalue.data() + svalue.size(), value);
    if (result.ec == std::errc::invalid_argument) {
	cout << "Could not convert.";
    }
    s.remove_prefix(pos+1);
    return s;
}

class Date {
public:
    Date() : day_(0), month_(0), year_(0) {}
    
    explicit Date(int day, int month, int year) :
	day_(day),
	month_(month),
	year_(year) {}
    
    explicit Date(string_view s) {
	s = extractInt(s, year_, '-');
	s = extractInt(s, month_, '-');
	s = extractInt(s, day_, '-');
    }
    
    time_t AsTimestamp() const {
	std::tm t;
	t.tm_sec   = 0;
	t.tm_min   = 0;
	t.tm_hour  = 0;
	t.tm_mday  = day_;
	t.tm_mon   = month_ - 1;
	t.tm_year  = year_ - 1900;
	t.tm_isdst = 0;
	return mktime(&t);
    }
    friend ostream& operator<<(ostream& os, const Date& dt);
    
private:
    int day_;
    int month_;
    int year_;
};

ostream& operator <<(ostream& out, const Date& date) {
    out << date.day_ << "-" << date.month_ << "-" << date.year_;
    return out;
}

int ComputeDaysDiff(const Date& date_to, const Date& date_from) {
  const time_t timestamp_to = date_to.AsTimestamp();
  const time_t timestamp_from = date_from.AsTimestamp();
  static const int SECONDS_IN_DAY = 60 * 60 * 24;
  return (timestamp_to - timestamp_from) / SECONDS_IN_DAY;
}

class Budget {
public:
    Budget() : data_(365*101, 0) {}

    void Earn(const Date& from, const Date& to, int value) {
	Date zero(1,1,2000);
	int idx_from = ComputeDaysDiff(from, zero);
	int idx_to = ComputeDaysDiff(to, zero);

	double daily_income = (value * 1.0)/(idx_to - idx_from + 1);
	transform(data_.begin() + idx_from,
		  data_.begin() + idx_to + 1,
		  data_.begin() + idx_from,
		  [daily_income] (double x) {return x + daily_income; });
    }
    
    double ComputeIncome(const Date& from, const Date& to) {
	Date zero(1,1,2000);
	int idx_from = ComputeDaysDiff(from, zero);
	int idx_to = ComputeDaysDiff(to, zero);
	return accumulate(data_.begin() + idx_from, data_.begin() + idx_to + 1, 0.);
    }

    void PayTax(const Date& from, const Date& to) {
	Date zero(1,1,2000);
	int idx_from = ComputeDaysDiff(from, zero);
	int idx_to = ComputeDaysDiff(to, zero);
	transform(data_.begin() + idx_from,
		  data_.begin() + idx_to + 1,
		  data_.begin() + idx_from,
		  [] (double x) {return x * 0.87; });
    }

    
private:
    vector<double> data_;
};


int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.precision(25);
    
    int n;
    string request;
    cin >> n;
    getline(cin, request);
    Budget budget;
    
    for (int i=0; i < n; ++i) {
	getline(cin, request);
	
	string_view str = request;
	
	size_t pos = str.find(' ');
	string_view type = str.substr(0, pos);
	str.remove_prefix(pos+1);
	pos = str.find(' ');
	Date from(str.substr(0, pos));
	str.remove_prefix(pos+1);
	pos = str.find(' ');
	Date to(str.substr(0, pos));
	
	if (type == "Earn") {
	    int value;
	    str.remove_prefix(pos+1);
	    extractInt(str, value, ' ');
	    budget.Earn(from, to, value);
	}
	else if (type == "ComputeIncome") {
	    cout << budget.ComputeIncome(from, to) << endl;
	}
	else if (type == "PayTax") {
	    budget.PayTax(from, to);
	}

	
    }
    return 0;
}
