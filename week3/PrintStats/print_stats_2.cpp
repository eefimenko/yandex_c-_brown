#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <numeric>

using namespace std;

template <typename Iterator>
class IteratorRange {
public:
  IteratorRange(Iterator begin, Iterator end)
    : first(begin)
    , last(end)
  {
  }

  Iterator begin() const {
    return first;
  }

  Iterator end() const {
    return last;
  }

private:
  Iterator first, last;
};

template <typename Collection>
auto Head(Collection& v, size_t top) {
  return IteratorRange{v.begin(), next(v.begin(), min(top, v.size()))};
}

struct Person {
  string name;
  int age, income;
  bool is_male;
};

vector<Person> ReadPeople(istream& input) {
  int count;
  input >> count;

  vector<Person> result(count);
  for (Person& p : result) {
    char gender;
    input >> p.name >> p.age >> p.income >> gender;
    p.is_male = gender == 'M';
  }

  return result;
}

string obtainMostPopularName(const vector<Person>& people,
			     const vector<int>& index,
			     IteratorRange<vector<int>::iterator> range) {
        
    if (range.begin() == range.end()) {
	return "";
    } else {
	sort(range.begin(), range.end(), [&people](int lhs, int rhs) {
		return people[lhs].name < people[rhs].name;
	    });
	const string* most_popular_name = &people[*range.begin()].name;
	int count = 1;
	for (auto i = range.begin(); i != range.end(); ) {
	    auto same_name_end = find_if_not(i, range.end(), [&people, i](int p) {
		    return people[p].name == people[*i].name;
		});
	    auto cur_name_count = std::distance(i, same_name_end);
	    if (cur_name_count > count) {
		count = cur_name_count;
		most_popular_name = &people[*i].name;
	    }
	    i = same_name_end;
	}
	return *most_popular_name;
    }
}

int main() {
    const vector<Person> people = []{
	vector<Person> people_ = ReadPeople(cin);
	sort(begin(people_), end(people_), [](const Person& lhs, const Person& rhs) {
		return lhs.age < rhs.age;
	    });
	return people_;
    }();
    
    const vector<int> wealthy_index = [&people]{
	vector<int> index(people.size());
	for (size_t i = 0; i < people.size(); ++i) {
	    index[i] = i;
	}
	sort(index.begin(), index.end(), [&people](int lhs, int rhs) {
		return people[lhs].income > people[rhs].income;
	    }
	    );
       	index[0] = people[index[0]].income;
	
	for (size_t i = 1; i < people.size(); ++i) {
	    index[i] = index[i-1] + people[index[i]].income;
	}

	return index;
    }();

    const pair<string, string> gender_index = [&people]{
	vector<int> index(people.size());
	for (size_t i = 0; i < people.size(); ++i) {
	    index[i] = i;
	}
	auto boundary = partition(begin(index), end(index), [&people](int p) {
		return people[p].is_male;
	    });
	
	string M_name = obtainMostPopularName(people, index, {begin(index), boundary});
	string F_name = obtainMostPopularName(people, index, {boundary, end(index)});
		
	return pair(M_name, F_name);
    }();
	

    for (string command; cin >> command; ) {
	if (command == "AGE") {
	    int adult_age;
	    cin >> adult_age;
	    
	    auto adult_begin = lower_bound(
		begin(people), end(people), adult_age, [](const Person& lhs, int age) {
		    return lhs.age < age;
		}
		);

	    cout << "There are " << std::distance(adult_begin, end(people))
		 << " adult people for maturity age " << adult_age << '\n';
	} else if (command == "WEALTHY") {
	    int count;
	    cin >> count;
	    cout << "Top-" << count << " people have total income " << wealthy_index[count-1] << '\n';
	} else if (command == "POPULAR_NAME") {
	    char gender;
	    cin >> gender;
	   	    
	    string mostPopularName = gender == 'M' ? gender_index.first : gender_index.second;
	    
	    if (mostPopularName.empty()) {
		cout << "No people of gender " << gender << '\n';
	    }
	    else {
		cout << "Most popular name among people of gender " << gender << " is "
		     << mostPopularName << '\n';
	    }
	    	    
	}
    }
}
