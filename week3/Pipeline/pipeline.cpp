#include "test_runner.h"
#include <functional>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

using namespace std;


struct Email {
    string from;
    string to;
    string body;
    Email() = default;
    Email(const string& from_, const string& to_, const string& body_) : from(from_), to(to_), body(body_) {}; 
};


class Worker {
public:
    virtual ~Worker() = default;
    virtual void Process(unique_ptr<Email> email) = 0;
    virtual void Run() {
	// только первому worker-у в пайплайне нужно это имплементировать
	throw logic_error("Unimplemented");
    }

protected:
    void PassOn(unique_ptr<Email> email) const {
	if (next_ != nullptr) {
	    next_->Process(move(email));
	}
    };

public:
    void SetNext(unique_ptr<Worker> next) {
	next_ = move(next);
    };
private:
    unique_ptr<Worker> next_ = nullptr;
};


class Reader : public Worker {
public:
    explicit Reader(istream& in) : in_(in) {};
    void Run() override {
	while (!in_.eof()) {
	    string from, to, body;
	    getline(in_, from);
	    if (in_.eof()) {
		return;
	    }
	    getline(in_, to);
	    if (in_.eof()) {
		return;
	    }
	    getline(in_, body);
	    
	    unique_ptr<Email> email = make_unique<Email>(from , to, body);
	    PassOn(move(email));
	}
    }
    void Process(unique_ptr<Email> email) override {};
private:
    istream& in_;
};


class Filter : public Worker {
public:
    using Function = function<bool(const Email&)>;

    explicit Filter(Function func) : func_(func) {};
    void Process(unique_ptr<Email> email) override {
	if (func_(*email)) {
	   PassOn(move(email)); 
	}
    };
private:
    Function func_;
};


class Copier : public Worker {
public:
    explicit Copier(const string& recepient) : copy_to_(recepient) {};
    void Process(unique_ptr<Email> email) override {
	if (copy_to_ != email->to) {
	    auto copy = make_unique<Email>(email->from, copy_to_,email-> body);
	    PassOn(move(email));
	    PassOn(move(copy));
	}
	else {
	    PassOn(move(email));
	}
	
    };
private:
    string copy_to_;
};


class Sender : public Worker {
public:
    explicit Sender(ostream& out) : out_(out) {};
    void Process(unique_ptr<Email> email) override {
	out_ << email->from << "\n"
	     << email->to   << "\n"
	     << email->body << "\n";
	PassOn(move(email));			       
    }
private:
    ostream& out_;
};


// реализуйте класс
class PipelineBuilder {
public:
    // добавляет в качестве первого обработчика Reader
    explicit PipelineBuilder(istream& in) {
	workers.push_back(make_unique<Reader>(in));
    };

    // добавляет новый обработчик Filter
    PipelineBuilder& FilterBy(Filter::Function filter) {
	workers.push_back(make_unique<Filter>(filter));
	return *this;
    };

    // добавляет новый обработчик Copier
    PipelineBuilder& CopyTo(string recipient) {
	workers.push_back(make_unique<Copier>(recipient));
	return *this;
    };

    // добавляет новый обработчик Sender
    PipelineBuilder& Send(ostream& out) {
	workers.push_back(make_unique<Sender>(out));
	return *this;
    };

    // возвращает готовую цепочку обработчиков
    unique_ptr<Worker> Build() {
	while (workers.size() > 1) {
	    auto tmp = move(workers.back());
	    workers.pop_back();
	    workers.back()->SetNext(move(tmp));
	}
	return move(workers.front());
   };
private:
    vector<unique_ptr<Worker>> workers;
};


void TestSanity() {
    string input = (
	"erich@example.com\n"
	"richard@example.com\n"
	"Hello there\n"

	"erich@example.com\n"
	"ralph@example.com\n"
	"Are you sure you pressed the right button?\n"

	"ralph@example.com\n"
	"erich@example.com\n"
	"I do not make mistakes of that kind\n"

	"erich@example.com\n"
	"richard@example.com"

	);
    istringstream inStream(input);
    ostringstream outStream;

    PipelineBuilder builder(inStream);
    builder.FilterBy([](const Email& email) {
	    return email.from == "erich@example.com";
	});
    builder.CopyTo("richard@example.com");
    builder.Send(outStream);
    auto pipeline = builder.Build();

    pipeline->Run();

    string expectedOutput = (
	"erich@example.com\n"
	"richard@example.com\n"
	"Hello there\n"

	"erich@example.com\n"
	"ralph@example.com\n"
	"Are you sure you pressed the right button?\n"

	"erich@example.com\n"
	"richard@example.com\n"
	"Are you sure you pressed the right button?\n"
	);

    ASSERT_EQUAL(expectedOutput, outStream.str());
}

int main() {
    TestRunner tr;
    RUN_TEST(tr, TestSanity);
    return 0;
}
