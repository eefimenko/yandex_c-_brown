#include "test_runner.h"

#include <algorithm>
#include <iostream>
#include <string>
#include <queue>
#include <stdexcept>
#include <unordered_set>

using namespace std;

template <class T>
class ObjectPool {
public:
    T* Allocate() {
	T* p;
	if (available.size() == 0)
	{
	    p = new T;
	    used.insert(p);
	}
	else
	{
	    p = available.front();
	    available.pop();
	    used.insert(p);
	}
	return p;
    }
  
    T* TryAllocate() {
	T* p = nullptr;
	if (available.size() != 0)
	{
	    p = available.front();
	    available.pop();
	    used.insert(p);
	}
	return p;
    }

    void Deallocate(T* object) {
	auto it = used.find(object);
	if (it == used.end()) {
	    throw invalid_argument("No active object");
	}
	else {
	    available.push(*it);
	    used.erase(it);
	}
    }

    ~ObjectPool() {
	for(auto it = used.begin(); it != used.end(); ++it)
	{
	    delete *it;
	}
	used.clear();
	
	while(!available.empty())
	{
	    delete available.front();
	    available.pop();
	}
    }

private:
    queue<T*> available;
    unordered_set<T*> used;
};

void TestObjectPool() {
  ObjectPool<string> pool;

  auto p1 = pool.Allocate();
  auto p2 = pool.Allocate();
  auto p3 = pool.Allocate();

  *p1 = "first";
  *p2 = "second";
  *p3 = "third";

  pool.Deallocate(p2);
  ASSERT_EQUAL(*pool.Allocate(), "second");

  pool.Deallocate(p3);
  pool.Deallocate(p1);
  ASSERT_EQUAL(*pool.Allocate(), "third");
  ASSERT_EQUAL(*pool.Allocate(), "first");

  pool.Deallocate(p1);
}

int main() {
  TestRunner tr;
  RUN_TEST(tr, TestObjectPool);
  return 0;
}
