#include "ini.h"

namespace Ini
{
    Section& Document::AddSection(string name) {
	auto result = sections.emplace(pair<string, Section>{name, Section()});
	return result.first->second;
    }

    const Section& Document::GetSection(const string& name) const {
	return sections.at(name);
    }
    
    size_t Document::SectionCount() const {
	return sections.size();
    }

    Document Load(istream& input) {
	Document result;
	string line;
	getline(input, line);
	
	while(line[0] == '[') {
	    auto pos = line.find_first_of(']');
	    string section = line.substr(1, pos-1);
	    //cout << "Section: " << section << endl;
	    Section& curr_section = result.AddSection(section);
	    while(getline(input, line))
	    {
		if (line.empty()) {
		    continue;
		}

		if (line[0] == '[') {
		    break;
		}
		
		auto pos = line.find_first_of('=');
		curr_section[line.substr(0, pos)] = line.substr(pos+1, string::npos); 
	    }
	}
	return result;
    }
    
} // Ini
