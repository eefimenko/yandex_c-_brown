#pragma once
#include <iostream>

namespace RAII {
    
template <typename T>
    class Booking {
public:
    Booking(T* provider, int counter)
        : provider_(provider),
	  counter_(counter)
    {
	//std::cout << "Ctor " << counter_ << std::endl;
    }

    Booking(const Booking&) = delete;
    Booking(Booking&& other)
	: provider_(other.provider_),
	  counter_(other.counter_)
    {
	other.provider_ = nullptr;	
    }

    Booking& operator=(const Booking&) = delete;
    Booking& operator=(Booking&& other)	
    {
	provider_ = other.provider_;
	counter_ = other.counter_;
	other.provider_ = nullptr;
	return *this;
    };

    ~Booking() {
	//std::cout << "Dtor " << counter_ << std::endl;
	if (provider_ != nullptr) {
	    provider_->CancelOrComplete(*this);
	}
    }
private:
    T* provider_;
    int counter_;
};
    
} // RAII
