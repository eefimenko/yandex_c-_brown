#include "Common.h"

using namespace std;

// Этот файл сдаётся на проверку
// Здесь напишите реализацию необходимых классов-потомков `IShape`

class Rectangle : public IShape {
public:
    unique_ptr<IShape> Clone() const override {
	auto shape = make_unique<Rectangle>();
	shape->SetPosition(position_);
	shape->SetSize(size_);
	shape->SetTexture(texture_);
	return shape;
    }
    
    void SetPosition(Point pos) override { position_ = move(pos); };
    Point GetPosition() const override { return position_; };

    void SetSize(Size size) override {size_ = move(size); };
    Size GetSize() const override { return size_; };

    void SetTexture(std::shared_ptr<ITexture> texture) override {
	texture_ = texture;
    };
    ITexture* GetTexture() const override { return texture_.get(); };

    void Draw(Image& image) const override {
	int nr = image.size();
	if (nr == 0) {
	    return;
	}
	int nc = image[0].size();
	if (nc == 0) {
	    return;
	}

	int tr = 0;
	int tc = 0;
	
	if (texture_ != nullptr) {
	    Size texture_size = texture_->GetSize();
	    tr = texture_size.height;
	    tc = texture_size.width;
	}
	
	int x0 = position_.x;
	int y0 = position_.y;
	
	for (int i = 0; i < size_.width; ++i) {
	    for (int j = 0; j < size_.height; ++j) {
		if (x0 + i >= nc || y0 + j >= nr) {
		    continue;
		}
		if (i < tc && j < tr) {
		    image[j + y0][i + x0] = texture_->GetImage()[j][i];
		}
		else {
		    image[j + y0][i + x0] = '.';
		}
	    }
	}
    };
private:
    Size size_;
    Point position_;
    shared_ptr<ITexture> texture_;
};

class Ellipse : public IShape {
public:
    unique_ptr<IShape> Clone() const override {
	auto shape = make_unique<Ellipse>();
	shape->SetPosition(position_);
	shape->SetSize(size_);
	shape->SetTexture(texture_);
	return shape;
    }
    
    void SetPosition(Point pos) override { position_ = move(pos); };
    Point GetPosition() const override { return position_; };

    void SetSize(Size size) override {size_ = move(size); };
    Size GetSize() const override { return size_; };

    void SetTexture(std::shared_ptr<ITexture> texture) override {
	texture_ = texture;
    };
    ITexture* GetTexture() const override { return texture_.get(); };

    void Draw(Image& image) const override {
	int nr = image.size();
	if (nr == 0) {
	    return;
	}
	int nc = image[0].size();
	if (nc == 0) {
	    return;
	}

	int tr = 0;
	int tc = 0;
	
	if (texture_ != nullptr) {
	    Size texture_size = texture_->GetSize();
	    tr = texture_size.height;
	    tc = texture_size.width;
	}
	
	int x0 = position_.x;
	int y0 = position_.y;
	
	for (int i = 0; i < size_.width; ++i) {
	    for (int j = 0; j < size_.height; ++j) {
		if (x0 + i >= nc || y0 + j >= nr) {
		    continue;
		}
		if (IsPointInEllipse({i,j}, size_)) {
		    if (i < tc && j < tr) {
			image[j + y0][i + x0] = texture_->GetImage()[j][i];
		    }
		    else {
			image[j + y0][i + x0] = '.';
		    }
		}
	    }
	}
    };
private:
    Size size_;
    Point position_;
    shared_ptr<ITexture> texture_;    
};

// Напишите реализацию функции
unique_ptr<IShape> MakeShape(ShapeType shape_type) {
    if (shape_type == ShapeType::Rectangle) {
	return make_unique<Rectangle>();
    }
    else {
	return make_unique<Ellipse>();
    }
}
